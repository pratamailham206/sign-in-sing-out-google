<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="google-signin-client_id" content="923428471981-eb2kss6mjqostkatn9kqqq04qbjtn9pn.apps.googleusercontent.com">
    <link rel="stylesheet" href="style.css">
    <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
    <title>GoogleIntegrate</title>
</head>
<body>
<section class="nav">
    <div class="toggle">
        <div class="nav2">
            <div class="bar">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
            <h1 class="text1">Sign In Google Integrate</h1>
        </div>
        <div class="image"></div>
        <div class="text">
            <div id="img1"></div>             
            <p id="login">LOGIN</p>
            <div class="box1">
            <p id="user">Username</p></div>
            <div class="box2">
            <p id="pass">Password</p></div>
            <div class="gogl"><div class="g-signin2" data-onsuccess="onSignIn"></div></div>
            <a><p class="out" onclick="signOut();">Sign Out</p></a>
        </div>        
    </div>
</section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script>    
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();            
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
            $("#img1").css("background", "url(" + profile.getImageUrl() +  ")");
            $("#img1").css("visibility", "visible");
            $("#login").css("visibility", "hidden");
            $("#user").text(profile.getName());
            $("#pass").text(profile.getEmail());
            $(".out").css("visibility", "visible");
        }

        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');
                });
            $("#img1").css("visibility", "hidden");
            $("#login").css("visibility", "visible");
            $("#user").text("Username");
            $("#pass").text("Password");
            $(".out").css("visibility", "hidden");
        }
    </script>   
</body>
</html>